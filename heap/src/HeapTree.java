import java.util.Random;

public class HeapTree {

    Random r = new Random(50);

    int size;

    private Nodo heap[];

    private static class Nodo {

        private int val;

        private int pos;

        public Nodo(int val, int pos){
            this.val = val<0?val*-1:val;
            this.pos = pos;
        }

        public Nodo(Nodo n){
            this.val = n.val;
            this.pos = n.pos;
        }

        public Nodo(){}

        public int getVal(){
            return val;
        }

    }

    public HeapTree(int n){
        heap = new Nodo[n];
        size = n;
        for (int i = 0; i < size; i++)
            heap[i] = new Nodo((int) (r.nextInt() * 0.00000001),i);
    }

    public HeapTree(int[]arr){
        heap = new Nodo[arr.length];
        size = arr.length;
        for(int i = 0; i < size; i++)
            heap[i] = new Nodo(arr[i],i);
    }

    public void printHeap(){
        int j = 0;
        for(int i=0;i<=size&&j+Math.pow(2,i)<=size;i++){
            System.out.println("Level " +(i+1));
            for(j=0;j<Math.pow(2,i)&&j+Math.pow(2,i)<=size;j++){
                System.out.print(heap[j+(int)Math.pow(2,i)-1].val+" ");
            }
            System.out.println();
        }

    }

    private void swap(Nodo i, Nodo j){
        //Nodo temp = heap[i.pos];
        Nodo temp = new Nodo(i);
        heap[i.pos].val = heap[j.pos].val;
        heap[j.pos].val = temp.val;
    }

    public void maxHeapify(int i){
        Nodo largest = heap[i];

        if (i*2+1 < size && heap[i*2+1].val > heap[i].val){
            largest = heap[i*2+1];
        }
        if (i*2+2 < size && heap[i*2+2].val > heap[i].val &&heap[i*2+2].val>largest.val ) {
            largest = heap[i*2+2];
        }
        if (largest.pos != i) {
            swap(heap[i], largest);
            maxHeapify(largest.pos);
        }

    }

    public void minHeapify(int i){
        Nodo largest = heap[i];

        if (i*2+1 < size && heap[i*2+1].val < heap[i].val){
            largest = heap[i*2+1];
        }
        if (i*2+2 < size && heap[i*2+2].val < heap[i].val &&heap[i*2+2].val>largest.val ) {
            largest = heap[i*2+2];
        }
        if (largest.pos != i) {
            swap(heap[i], largest);
            minHeapify(largest.pos);
        }
    }

    public void maxHeapSort(){
        for(int i = size/2-1;i>=0;i--)
            minHeapify(i);
        int oldSize = size;
        for (size-=1; size>=0; size--)
        {
            swap(heap[0], heap[size]);
            minHeapify(0);
        }
        size = oldSize;//essendo che lavoro con un parametro dell'heap e voglio comunque mantenerlo nel
        //tempo per altre operazioni, sfrutto questa variabile di supporto
    }

    public void minHeapSort(){
        for(int i = size/2-1;i>=0;i--)
            maxHeapify(i);
        int oldSize = size;
        for (size-=1; size>=0; size--)
        {
            swap(heap[0], heap[size]);
            maxHeapify(0);
        }
        size = oldSize;//essendo che lavoro con un parametro dell'heap e voglio comunque mantenerlo nel
                       //tempo per altre operazioni, sfrutto questa variabile di supporto
    }

}
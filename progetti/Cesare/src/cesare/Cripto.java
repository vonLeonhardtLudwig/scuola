package cesare;

import java.util.Random;


public class Cripto {

	
	private String message;
	private int spostamento;
	private static int max = 122;
	private static int min = 96;
	private static int maiuscMinusc = 35;
	private static Random rng = new Random();

	public Cripto(){
		spostamento = rng.nextInt((5 - 2) + 1) + 2;
		if(!FileChiave.hasBeenInit()){
			FileChiave.init();
		}
		FileChiave.write(Integer.toString(spostamento));
	}

	public String cripta(String word) {
		char[] mex = new char[word.length()];
		for (int i = 0; i < word.length(); i++) {
			mex[i] = (char) (((int) word.charAt(i) + spostamento > max ? min + (spostamento - (max - (int) word.charAt(i))) : (int) word.charAt(i) + spostamento));
		}
		message = new String(mex);
		return message;
	}

	public int rndOffset() {
		spostamento = rng.nextInt((5 - 2) + 1) + 2;
		return spostamento;
	}

	public boolean setOffset(int off) {
		spostamento = off;
		return spostamento == off;
	}

	public int getSliding() {
		return spostamento;
	}

	public String getMessage() {
		return message;
	}

}

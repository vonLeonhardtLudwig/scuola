package cesare;

public class Decripto {

	private String decrip;
	private int spostamento;
	private static int max = 122;
	private static int min = 96;
	private static int maiuscMinusc = 35;

	Decripto() {
		char c;
		spostamento = Integer.parseInt(String.valueOf(FileChiave.read().charAt(0)));
		if (!FileChiave.hasBeenInit()) {
			FileChiave.init();
		}
	}

	public String decripta(String word) {
		char[] mex = new char[word.length()];
		for (int i = 0; i < word.length(); i++) {
			if ((int)word.charAt(i)>min&&(int)word.charAt(i)<max ) {
				mex[i] = (char) (word.charAt(i) - spostamento);
				if ((int) (mex[i]) < (int) 'a') {
					mex[i] = (char) (mex[i] + ('z' + 1 - 'a'));
				}
			} else {
				mex[i]= ' ';
			}

			//mex[i] = (char) (((int) word.charAt(i) - spostamento < min ? max - (spostamento + (max - (int) word.charAt(i))) : (int) word.charAt(i) - spostamento));
			//System.out.println(word.charAt(i));
			//(((int)word.charAt(i))-spostamento<min?max-((int)word.charAt(i))-((int)word.charAt(i-spostamento)):spostamento)
			//mex[i]= (char)((int) word.charAt(i) - (((int)word.charAt(i))-spostamento<min?max-((int)word.charAt(i))-((int)word.charAt(i-spostamento)):spostamento));
		}

		//System.out.println(spostamento);
		decrip = new String(mex);
		return decrip;
	}
}

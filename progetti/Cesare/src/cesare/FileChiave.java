/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cesare;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileChiave {
	private static boolean isInit = false;
	private static String fileName = "key.txt";
	private static PrintWriter writer;
	private static Scanner reader;
	protected static File file = new File(fileName);

	
	private static void initWriter() throws UnsupportedEncodingException {
		try {
			writer = new PrintWriter(fileName, "UTF-8");
		} catch (FileNotFoundException e) {
			System.out.println("FILE DOESN'T EXIST");
		};
	}

	private static void initReader() throws FileNotFoundException {
		reader = new Scanner(file);
	}

	public static boolean hasBeenInit() {
		return isInit;
	}

	public static boolean init() {
		try {
			initWriter();
		} catch (UnsupportedEncodingException e) {
			System.out.println("Error");
		};
		try {
			initReader();
		}catch (FileNotFoundException e) {
			System.out.println("File not found");
		};
		isInit = true;
		return isInit;
	}

	public static String read(){
		return reader.nextLine();
	}
	
	public static void write(String s){
		writer.println(s);
		writer.close();
	}
}

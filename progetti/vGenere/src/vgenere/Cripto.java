package vgenere;

public class Cripto {
	private String worm;
	private String message;
	private final static String charToUse = "ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwyxz +-,.!?éèòàùì";

	public Cripto(String worm) {
		this.worm = worm;
	}

	public String cripta(String word) {
		int mexLen = word.length();
		char[] mex = new char[mexLen];
		char c;
		for (int i = 0; i < mexLen; i++) {
			int sommaAscii = charToUse.indexOf(word.charAt(i)) + charToUse.indexOf(worm.charAt(i % worm.length()));
			sommaAscii = (sommaAscii < charToUse.length()) ? sommaAscii : sommaAscii - charToUse.length();
			c = charToUse.charAt(sommaAscii);
			mex[i] = c;
		}
		message = new String(mex);
		return message;
	}

	public boolean setWorm(String worm) {
		this.worm = worm;
		return this.worm.equals(worm);
	}

	public String getWorm() {
		return worm;
	}

	public String getMessage() {
		return message;
	}
}

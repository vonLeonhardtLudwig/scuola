import java.awt.image.BandCombineOp;
public class Main {

    public static void main(String[] args)
    {
        BankAccount conto = new BankAccount(1234,500);
        conto.deposit(500);
        conto.withdraw(300);
        conto.deposit(10000000);
        conto.withdraw(100);
        //System.out.println(conto.toStringLastAction());
        System.out.println(conto.toStringLastAction());
        conto.printAllActions();
    }
}

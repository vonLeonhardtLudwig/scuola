import java.text.SimpleDateFormat;
import java.util.*;
import java.lang.UnsupportedOperationException;

public class BankAccount {

    private long number;

    private long balance;

    private int nAct = 0;

    private int pos = 0;

    private ArrayList<Action> allActions = new ArrayList<>();

    private AccIterator iter = new AccIterator();

    BankAccount(){
        Random r = new Random(50);
        this.number = r.nextLong();
        this.balance = r.nextLong();
    }

    BankAccount(long number, long balance){
        this.number = number;
        this.balance = balance;
    }

    private class Action {

        Calendar cal;

        private int actPos;

        private String act;

        String timeStamp;

        private long amount;

        Action(String act, long amount) {

            this.act = act;

            this.amount = amount;

            this.actPos = nAct++;

            timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());


        }

        @Override
        public String toString() {
            return number + ": " + id + ": " + act + " " + amount + " " + timeStamp;
        }
    }

    public class AccIterator implements Iterator<Action>{

        @Override
        public boolean hasNext() {
            return pos<allActions.size();
        }

        @Override
        public Action next() throws NoSuchElementException {
            return allActions.get(pos++);
        }

        @Override
        public void remove() throws UnsupportedOperationException{
            throw new UnsupportedOperationException( "not implemented" );
        }
    }

    void deposita(long amount) {

        balance += amount;
        allActions.add(new Action("deposita",amount));
        //lastAct = new Action("deposit", amount);

    }

    void prelievo(long amount) {

        balance -= amount;
        allActions.add(new Action("prelievo",amount));
        //lastAct = new Action("withdraw", amount);

    }

    Action ultimaOperazione(){
        return allActions.get(allActions.size()-1);
    }

    String toStringUltimaAzione(){
        return ultimaOperazione().toString();
    }

    void printTutteOperazioni(){
        AccIterator it = new AccIterator();
        while(it.hasNext()){
            System.out.println(it.next().toString());
        }

    }

    public long estrattoConto(){
        return balance;
    }


}
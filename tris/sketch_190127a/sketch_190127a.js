var centre;
var squares = [];
var bigSquareLength; //lunghezza lati
var percentage = 80/100; //percentuale dimensione lati in proporzione alla finestra
function setup() {
  centre = {x: windowWidth/2, y: windowHeight/2 };
  var start = {x: centre.x-parseFloat(bigSquareLength/2), y:centre.y+parseFloat(bigSquareLength/2)}; 
  console.log(parseFloat(centre.x-bigSquareLength/2));
  createCanvas(windowWidth, windowHeight);
  background(51);
  bigSquareLength = windowHeight<=windowWidth?windowHeight*percentage:windowWidth*percentage;
  rectMode(CORNER);
  stroke(255);
  noFill();
  for(var r = 0; r<3; r++){ //r = row
    for(var c = 0; c<3; c++){ //c = column
      //console.log(start.x + parseFloat(bigSquareLength/3*c));
      //console.log(start.y + parseFloat(bigSquareLength/3*r));
      squares.push(new Square({x:start.x+parseFloat(bigSquareLength/3*c),y:start.y+parseFloat(bigSquareLength/3*r)},parseFloat(bigSquareLength/3)));  
    }
  }
}

function draw() {
  for(var i = 0;i < 9; i++){
    squares[i].drawSquare;
  }
}

function Square(coordinates, dim){
  this.x = coordinates.x;
  this.y = coordinates.y;
  this.drawSquare = function(){
    rect(this.x,this.y,dim,dim);  
  }
}
package server;

import java.io.*;
import java.net.*;

public class Server {

	private final int maxConnections = 10;
	private final int port = ServerMain.port;
	
	private ServerSocket serverSocket;
	private Socket socket;
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;
	
	private final Decripto deCripto = new Decripto();
	
	public Server() throws IOException, ClassNotFoundException{
		try{
			serverSocket = new ServerSocket(port,maxConnections);
			listen();
			createStreams();
			initProcessing();
		}
		finally{close();}
	}
	
	private void listen() throws IOException{
		socket = serverSocket.accept();
	}
	
	private void createStreams()throws IOException{
		outputStream = new ObjectOutputStream(socket.getOutputStream());
		outputStream.flush();
		inputStream = new ObjectInputStream(socket.getInputStream());
	}
	
	private void initProcessing()throws IOException, ClassNotFoundException{
		String CriptedClientMsg ="";
		String DeCriptedClientMsg ="";
		sendDataToClient("SERVER->Ciao, digita BYE per terminare...");
		do{
			CriptedClientMsg = (String)inputStream.readObject();
			DeCriptedClientMsg = deCripto.decripta(CriptedClientMsg);
			ServerMain.showMessage("CLIENT-> "+ DeCriptedClientMsg);
			sendDataToClient(CriptedClientMsg);
		}while(!DeCriptedClientMsg.trim().equals("BYE"));
	}
	
	private void sendDataToClient(String msgData)throws IOException{
		outputStream.writeObject(msgData);
		outputStream.flush();
	}
	
	
	
	public void close()throws IOException{
		ServerMain.showMessage("SERVER -> CHIUSURA CONNESSIONE");
		if(outputStream != null && inputStream != null && socket != null){
			outputStream.close();
			inputStream.close();
			socket.close();
					
		}
	}
	
}

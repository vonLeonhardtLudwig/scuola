package multiserver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import multiserver.ServerThread;

public class MultiServer {
	private final int maxConnections = 10;
	private final int port = ServerMain.port;
	
	private ServerSocket serverSocket;
	private Socket socket;
	
	public MultiServer(){
		serverSocket = null;
        try
        {
            serverSocket = new ServerSocket(30000);
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            System.exit(-1);
        }
        System.out.println("SERVER -> IN ATTESA DI CONNESSIONI...");
		
		while(true) {
            try
            {
                Socket socket = serverSocket.accept(); // pongo il server in attesa di connessioni
                ServerThread serverThread = new ServerThread(socket);
                Thread worker = new Thread(serverThread);
                worker.start();
            }
            catch(IOException ex)
            {
                ex.printStackTrace();
            }
        }
	}
}

package multiserver;

import java.io.*;
import java.net.Socket;

public class ServerThread implements Runnable {

	private Socket client = null;
	private String clientIp = null;
	private final Decripto deCripto = new Decripto();
	public ServerThread(Socket client) {
		this.client = client;
		clientIp = this.client.getInetAddress().getHostAddress();
		System.out.println("[" + clientIp + "] " + ">> Connessione in ingresso <<");
	}

	@Override
	public void run() {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		ObjectOutputStream outputStream = null;
		ObjectInputStream inputStream = null;
		try {
			outputStream = new ObjectOutputStream(client.getOutputStream());
			outputStream.flush();

			inputStream = new ObjectInputStream(client.getInputStream());
			System.out.println("SERVER -> STREAM CREATI");

			String CriptedClientMsg = "";
			String DeCriptedClientMsg = "";
			outputStream.writeObject("SERVER -> Ciao digita BYE per terminare...");
			outputStream.flush();

			do {
				CriptedClientMsg = (String) inputStream.readObject();
				DeCriptedClientMsg = deCripto.decripta(CriptedClientMsg);
				ServerMain.showMessage("CLIENT-> " + DeCriptedClientMsg);
				outputStream.writeObject(CriptedClientMsg);
				outputStream.flush();
			} while (!DeCriptedClientMsg.trim().equals("BYE"));
		} catch (IOException e) {

		} catch (ClassNotFoundException e) {

		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
			}
			System.out.println("SERVER -> CHIUSURA CONNESSIONE SOCKET [" + clientIp + "]");

		}
	}


}

package client;


public class Decripto {
	private String worm;
	private String message;
	private final static String charToUse = "ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwyxz +-,.!?éèòàùì";
	
	public Decripto() {
		this.worm = ClientMain.worm;
	}
	
	public String decripta(String word){
		int mexLen = word.length();
		char[] mex = new char[mexLen];
		char c;
		for(int i = 0; i<mexLen;i++){
			int sommaAscii = charToUse.indexOf(word.charAt(i))-charToUse.indexOf(worm.charAt(i%worm.length()));
			
			sommaAscii=(sommaAscii > 0)?sommaAscii:sommaAscii+charToUse.length();
			char sommaChar = charToUse.charAt(sommaAscii);
			mex[i]= sommaChar;

		}
		message = new String(mex);
		return message;
	}
	
}

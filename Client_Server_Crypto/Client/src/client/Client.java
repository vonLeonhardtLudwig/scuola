
package client;
import java.io.*;
import java.net.*;
import java.util.Scanner;
public class Client {

	private final int port = ClientMain.port;
	private final String remoteHostIp = ClientMain.remoteHostIp;
	private Socket socket;
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;
	
	private final Cripto cripto = new Cripto();
	private final Decripto deCripto = new Decripto();
	
	public Client()throws IOException,ClassNotFoundException{
		try{
			attemptToConnect();
			createStreams();
			initProcessing();
		}finally{close();}
	
	}
	
	private void attemptToConnect() throws IOException{
		ClientMain.showMessage("CLIENT-> CONNESSIONE VERSO UN SERVER...");
		socket = new Socket(remoteHostIp,port);
		ClientMain.showMessage("CLIENT->CONNESSIONE AVVENUTA VERSO "+socket.getInetAddress()
				+" ALLA PORTA REMOTA "+socket.getPort()
				+" E ALLA PORTA LOCALE "+socket.getLocalPort());
	}
	
	private void createStreams()throws IOException{
		outputStream = new ObjectOutputStream(socket.getOutputStream());
		outputStream.flush(); //libera il buffer
		
		inputStream = new ObjectInputStream(socket.getInputStream());
		ClientMain.showMessage("CLIENT->STREAM CREATI");
	}
	
	private void initProcessing() throws IOException, ClassNotFoundException{
		String msg ="";
		String criptedMsg ="";
		String criptedServerMsg = "";
		String deCriptedServerMsg = "";
		Scanner t = new Scanner(System.in);
		deCriptedServerMsg = (String) inputStream.readObject();
		ClientMain.showMessage(deCriptedServerMsg);
		do{
			
			msg = t.nextLine();
			criptedMsg = cripto.cripta(msg);
			sendDataToServer(criptedMsg);

			criptedServerMsg = (String) inputStream.readObject();
			deCriptedServerMsg = deCripto.decripta(criptedServerMsg);
			ClientMain.showMessage("SERVER ->"+deCriptedServerMsg);
		}while(!msg.equals("BYE"));
		
		
	}
	
	private void sendDataToServer(String msgData)throws IOException{
		outputStream.writeObject(msgData);
		outputStream.flush();
	}
	
	private void close()throws IOException{
		ClientMain.showMessage("CHIUSURA CONNESSIONE");
		if(outputStream != null && inputStream != null && socket != null){
			outputStream.close();
			inputStream.close();
			socket.close();	
		}
	}
}

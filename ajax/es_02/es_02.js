let n_imgs = 15
let imgs = []
let findThis = "vaporwave"

function get_n_imgs() {
  let txt;
  let n = prompt("Please enter the number of pics you want:", "15");
  if (n == null || n == "") {
    txt = "Default number of images: 15";
  } else {
    txt = n+" images will be loaded for you";
    n_imgs=n
  }

}

function get_type_of_imgs(){

  let txt;
  let pic = prompt("Please enter which kind of pics you want:", "Vaporwave");
  if (pic == null || pic == "") {
    txt = "Default type of pic is vaporwave.";
  } else {
    txt = pic +" pics will be loaded for you";
    findThis=pic
  }

}
window.onload = function() {
  get_type_of_imgs()
  get_n_imgs()
  let gallery = document.getElementById("gallery");
  for(let i = 0; i<n_imgs;i++){

    let url = "https://api.unsplash.com/search/photos?client_id=f3568b550d2303741855fc9d4a8ece4bac7e4b23e41e5fa61a55f891eca1a378&query="+findThis;

    let request = new XMLHttpRequest();

    request.onreadystatechange = function(){
      if (request.readyState === 0)
        console.log("UNSENT");
      if (request.readyState === 1)
        console.log("OPENED");
      if (request.readyState === 2)
        console.log("HEADER RECEIVD");
      if (request.readyState === 3)
        console.log("LOADING");
      if (request.readyState === 4) {
        console.log("DONE")
      if (request.status === 200){
          let response = JSON.parse(request.responseText);
          //imgs[i] = document.getElementsByClassName('ajax_img')[i];
          imgs[i]=document.createElement('img');
          imgs[i].setAttribute("class","ajax_img");
          gallery.appendChild(imgs[i])
          let rnd;
          do{
            rnd = Math.floor(Math.random()*n_imgs)
          }while(response.results[rnd]==undefined)
          imgs[i].src = response.results[rnd].urls.full;
      }
    }
  }

  request.open('GET', url, true);
  request.send();
  // add listener
  }
}

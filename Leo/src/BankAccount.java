import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.NoSuchElementException;
import java.lang.UnsupportedOperationException;

public class BankAccount {

    private long number;

    private long balance;

    private int nAct = 0;

    private int pos = 0;

    private ArrayList<Action> allActions = new ArrayList<>();

    private AccIterator iter = new AccIterator();

    BankAccount(){
        Random r = new Random(50);
        this.number = r.nextLong();
        this.balance = r.nextLong();
    }

    BankAccount(long number, long balance){
        this.number = number;
        this.balance = balance;
    }

    private class Action {

        private int actPos;

        private String act;

        private long amount;

        Action(String act, long amount) {

            this.act = act;

            this.amount = amount;

            this.actPos = nAct++;

        }

        @Override
        public String toString() {
            return number + ": " + act + " " + amount;
        }
    }

    public class AccIterator implements Iterator<Action>{

        @Override
        public boolean hasNext() {
            return pos<allActions.size();
        }

        @Override
        public Action next() throws NoSuchElementException {
            return allActions.get(pos++);
        }

        @Override
        public void remove() throws UnsupportedOperationException{
            throw new UnsupportedOperationException( "not implemented" );
        }
    }

    public void deposit(long amount) {

        balance += amount;
        allActions.add(new Action("deposit",amount));
        //lastAct = new Action("deposit", amount);

    }
    public void withdraw(long amount) {

        balance -= amount;
        allActions.add(new Action("withdraw",amount));
        //lastAct = new Action("withdraw", amount);

    }

    public Action lastAction(){
        return allActions.get(allActions.size()-1);
    }

    public String toStringLastAction(){
        return lastAction().toString();
    }


    public void printAllActions(){
        AccIterator it = new AccIterator();
        while(allActions.iterator().hasNext()){
            it.next().toString();
                //toString();

        }

    }

}
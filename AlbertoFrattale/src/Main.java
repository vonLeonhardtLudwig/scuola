
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Albero Frattale");
        StackPane layout = new StackPane();
        Scene scene = new Scene(layout,800,800);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

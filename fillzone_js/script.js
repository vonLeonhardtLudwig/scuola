//15 x 15
//let colors = ['#ff33ac','#ff0000','#00ff00','#0000ff','#33a8ff','#ff7c00']
//['rgb(255,51,172)','rgb(255,0,0)','rgb(0,255,0)','rgb(0,0,255)','rgb(51,168,255)','rgb(255,124,0)']
let palette = new Palette(['#ff33ac','#ff0000','#00ff00','#0000ff','#33a8ff','#ff7c00'])
let size = 15
let tabella =new Table(size,size)
let clicked
let prevTab
let moves = 30
function Palette(colors){
  this.setColors = function (colors){
    let c = new Array(colors.length)
    for(let i = 0; i< c.length; i++){
        c[i]=colors[i]
    }
    return c
  }
  this.colors=this.setColors(colors)
  this.initialize_palette = function (n_colors){
      let body = document.getElementsByTagName("body")[0]
      let row = document.createElement('row')
      body.appendChild(row)
      let tr = document.createElement('TR')
      row.appendChild(tr)
      for(let i = 0; i<n_colors;i++){

        let td = document.createElement('TD')
        td.style.backgroundColor = palette.colors[i]
        td.classList.add(i)
        td.onclick = function(event){
          clicked = event.target.classList.item(0)
          prevTab = tabella.matrix[0][0].style.backgroundColor
          colorFill(0,0)
          console.log(moves)
          if(--moves==0){
              console.log("you lost")
          }
        }
        tr.appendChild(td)
      }
  }
}

function colorFill(r,c){
  if(c-1>=0 && prevTab==tabella.matrix[r][c-1].style.backgroundColor){ //c-1>=0
      tabella.matrix[r][c-1].style.backgroundColor = palette.colors[clicked]
      colorFill(r,c-1)
  }
  if(c+1<size && prevTab==tabella.matrix[r][c+1].style.backgroundColor){ //c+1<size
    tabella.matrix[r][c+1].style.backgroundColor = palette.colors[clicked]
    colorFill(r,c+1)
  }
  if(r+1<size && prevTab==tabella.matrix[r+1][c].style.backgroundColor){ //r+1<size
    tabella.matrix[r+1][c].style.backgroundColor = palette.colors[clicked]
    colorFill(r+1,c)
  }
  if(r-1>=0 && prevTab==tabella.matrix[r-1][c].style.backgroundColor){ //r-1>=0
    tabella.matrix[r+1][c].style.backgroundColor = palette.colors[clicked]
    colorFill(r-1,c)
  }
  tabella.matrix[r][c].style.backgroundColor=palette.colors[clicked]
  //aggiungo +1 perché ho 16 tr, il tr = 0 è la palette
  document.getElementsByTagName('TR')[r+1].childNodes[c].style.backgroundColor = palette.colors[clicked]
}

function Table(r,c){
  this.create_matrix = function(r,c){
    let m = []
    for(let y = 0;y<r;y++){
      m[y] =[];
      for(let x = 0; x<c; x++){
        m[y][x]=null;
      }
    }
    return m
  }
  this.matrix = this.create_matrix(r,c)
  this.initialize_table = function (n_rows,n_columns) {
    let body = document.getElementsByTagName("body")[0]
    let table = document.createElement('TABLE')
    let tblB = document.createElement('TBODY') //body tabella
    //append tablebody to tabl
    body.appendChild(table)
    table.appendChild(tblB)
    for(let i = 0; i<n_rows;i++){
      //crea righe
      let tr = document.createElement('TR')
      tblB.appendChild(tr)
      for(let j = 0; j<n_columns;j++){
        //crea celle
        let td = document.createElement('TD')
        td.style.backgroundColor = generateColor()

        tabella.matrix[i][j]=td
        tr.appendChild(td)
      }

    }
    body.appendChild(table)
  }
}


function generateColor(){
  let min=0
  let max=palette.colors.length-1
  let random = parseInt(Math.random() * (+max - +min)+min)
  return palette.colors[random]
}

window.onload=function(){
  console.log(moves)
  palette.initialize_palette(palette.colors.length)
  tabella.initialize_table(size,size)
}

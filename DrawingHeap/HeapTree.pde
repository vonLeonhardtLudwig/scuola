
import java.util.*;
class HeapTree{
  private int heapHeight;
  private final int FACTOR = 3; //MOLTIPLICATO PER IL DIAMETRO INDICA QUANTO UN NODO ED UN POSSIBILE RISPETTIVO ARCO OCCUPINO SPAZIO
  private float d;
  private int txtSize;
    private void setupSettings(){
      this.heapHeight=(int)(Math.log(size)/Math.log(2))+1;
      this.d=(float)(width/Math.pow(2,heapHeight-1));
      this.txtSize=int(d-d/2);
      textSize(txtSize);
    }
  
    public void drawHeap(){
      float angle = 45;
      float x = width/2;
      float y = 800-(height-d/2);
      drawNode(x,y,heap[0].getVal());
      
    }
    private void drawNode(float x, float y, int n){
      fill(0,150,255);
      stroke(0,50,255);
      ellipse(x,y,this.d,this.d);
      String s = str(n);
      fill(255);
      text(s, x-(txtSize/2), y+(txtSize/2));
    }
  
    Random r = new Random();

    int size;

    private class Nodo{
        private int val;

        private int pos;

        public Nodo(int val, int pos){
            this.val = val<0?val*-1:val;
            this.pos = pos;
        }

        public Nodo(Nodo n){
            this.val = n.val;
            this.pos = n.pos;
        }

        public Nodo(){}

        public int getVal(){
            return val;
        }

    }
    
    private class InOrder implements Iterator<Nodo>{
        InOrder(Nodo root){
            actual = root;
            stack = new Stack<>();
        }
        private Stack<Nodo> stack;
        private Nodo actual;
        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        @Override
        public Nodo next() {
            Nodo temp;
            actual = stack.pop();
            temp = actual;
            actual = actual.p;
            return temp;
        }
    }

    private Nodo heap[];
    
    public HeapTree(int n){
        heap = new Nodo[n];
        size = n;
        for (int i = 0; i < size; i++)
            heap[i] = new Nodo((int) (r.nextInt() * 0.00000001),i);
        setupSettings();
    }

    public HeapTree(int[]arr){
        heap = new Nodo[arr.length];
        size = arr.length;
        for(int i = 0; i < size; i++)
            heap[i] = new Nodo(arr[i],i);
        setupSettings();
    }

    public void printHeap(){
        int j = 0;
        for(int i=0;i<=size&&j+Math.pow(2,i)<=size;i++){
            System.out.println("Level " +(i+1));
            for(j=0;j<Math.pow(2,i)&&j+Math.pow(2,i)<=size;j++){
                System.out.print(heap[j+(int)Math.pow(2,i)-1].val+" ");
            }
            System.out.println();
        }

    }

    private void swap(Nodo i, Nodo j){
        //Nodo temp = heap[i.pos];
        Nodo temp = new Nodo(i);
        heap[i.pos].val = heap[j.pos].val;
        heap[j.pos].val = temp.val;
    }

    public void maxHeapify(int i){
        Nodo largest = heap[i];

        if (i*2+1 < size && heap[i*2+1].val > heap[i].val){
            largest = heap[i*2+1];
        }
        if (i*2+2 < size && heap[i*2+2].val > heap[i].val &&heap[i*2+2].val>largest.val ) {
            largest = heap[i*2+2];
        }
        if (largest.pos != i) {
            swap(heap[i], largest);
            maxHeapify(largest.pos);
        }

    }

    public void minHeapify(int i){
        Nodo largest = heap[i];

        if (i*2+1 < size && heap[i*2+1].val < heap[i].val){
            largest = heap[i*2+1];
        }
        if (i*2+2 < size && heap[i*2+2].val < heap[i].val &&heap[i*2+2].val>largest.val ) {
            largest = heap[i*2+2];
        }
        if (largest.pos != i) {
            swap(heap[i], largest);
            minHeapify(largest.pos);
        }
    }

    public void maxHeapSort(){
        for(int i = size/2-1;i>=0;i--)
            minHeapify(i);
        int oldSize = size;
        for (size-=1; size>=0; size--)
        {
            swap(heap[0], heap[size]);
            minHeapify(0);
        }
        size = oldSize;//essendo che lavoro con un parametro dell'heap e voglio comunque mantenerlo nel
        //tempo per altre operazioni, sfrutto questa variabile di supporto
    }

    public void minHeapSort(){
        for(int i = size/2-1;i>=0;i--)
            maxHeapify(i);
        int oldSize = size;
        for (size-=1; size>=0; size--)
        {
            swap(heap[0], heap[size]);
            maxHeapify(0);
        }
        size = oldSize;//essendo che lavoro con un parametro dell'heap e voglio comunque mantenerlo nel
                       //tempo per altre operazioni, sfrutto questa variabile di supporto
    }

}
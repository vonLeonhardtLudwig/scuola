HeapTree heap;
void setup(){
  heap = new HeapTree(15);
  size(800,800);
  background(51);
}

void draw(){
  heap.printHeap();
  heap.drawHeap();
  noLoop();
}
let express = require('express')
let fs = require('fs')
let multer = require('multer')
let upload = multer({ dest: 'uploads/' })
let cors = require('cors')
let querystring = require('querystring')
let http = require('http')

let app = express()
app.use(express.static('uploads'),); //definisco cartella statica per i file
app.set('view engine', 'ejs'); //definisco l'engine
app.options('*',cors())

app.get('/',(req,res)=>{//DOWNLOAD PAGINA
    let query = req.query
    fs.readdir('uploads/', function(err, files){
        res.render('page', {title: 'Page',name:query.name,surname:query.surname});
      })
})

app.post('/upload', upload.single('photo'), (req, res) => {
    // req.file is the `photo` file
    // req.body will hold the text fields, if there were any
    console.log('upload file');
    res.redirect('/');
})

app.post('/upload', upload.single('photo'), (req, res) => {
  // req.file is the `photo` file
  // req.body will hold the text fields, if there were any
  console.log('upload file');
  res.redirect('/');
})


app.listen(8080,()=>{//TENERE SERVER IN ASCOLTO
    console.log("Server running at 127.0.0.1:8080")
})
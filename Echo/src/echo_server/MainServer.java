
package echo_server;

import java.io.IOException;
import java.util.Scanner;


public class MainServer {

	private static Scanner t = new Scanner(System.in);
	
	protected static final int port = 30000;
	
	protected static void showMessage(String msg){
		System.out.println(msg);
	}
	
	protected static String readFromInput(){
		return t.nextLine();
	}
	
	public static void main(String[] args) throws ClassNotFoundException,IOException {
		Server s = new Server();
	}
	
}

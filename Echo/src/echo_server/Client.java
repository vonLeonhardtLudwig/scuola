
package echo_server;

import java.io.*;
import java.net.*;

public class Client {
	private final int port = MainServer.port;
	private Socket socket;
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;
	
	public Client()throws IOException,ClassNotFoundException{
		try{
			attemptToConnect();
			createStreams();
			initProcessing();
		}finally{close();}
	
	}
	
	private void attemptToConnect() throws IOException{
		MainServer.showMessage("CLIENT-> CONNESSIONE VERSO UN SERVER...");
		socket = new Socket("localhost",port);
		MainServer.showMessage("CLIENT->CONNESSIONE AVVENUTA VERSO "+socket.getInetAddress()
				+" ALLA PORTA REMOTA "+socket.getPort()
				+" E ALLA PORTA LOCALE "+socket.getLocalPort());
	}
	
	private void createStreams()throws IOException{
		outputStream = new ObjectOutputStream(socket.getOutputStream());
		outputStream.flush(); //libera il buffer
		
		inputStream = new ObjectInputStream(socket.getInputStream());
		MainServer.showMessage("CLIENT->STREAM CREATI");
	}
	
	private void initProcessing() throws IOException, ClassNotFoundException{
		String serverMsg = (String) inputStream.readObject();
		MainServer.showMessage(serverMsg);
		do{
			outputStream.writeObject(MainServer.readFromInput()); 
			outputStream.flush();
			serverMsg = (String)inputStream.readObject();
			MainServer.showMessage(serverMsg);
		}while(!serverMsg.contains("BYE"));
	}
	
	private void close()throws IOException{
		MainServer.showMessage("CLIENT -> CHIUSURA CONNESSIONE");
		if(outputStream != null && inputStream != null && socket != null){
			outputStream.close();
			inputStream.close();
			socket.close();
					
		}
	}
	
	
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eserciziopil;

/**
 *
 * @author leonardo.pepe
 */
class Azienda {

    String ragioneSociale;
    double salari;
    double profitti;
    double fatturato;
    double spesa;
    boolean mercato;

    Azienda(String ragioneSociale, double salari, double profitti, double fatturato, boolean mercato) {
        this.ragioneSociale = ragioneSociale;
        this.salari = salari;
        this.profitti = profitti;
        this.fatturato = fatturato;
        this.spesa = fatturato - (profitti + salari);
        this.mercato = mercato;
    }
}

package eserciziopil;

import java.util.*;

public class CalcoloPIL {

    Scanner tastiera = new Scanner(System.in);
    Vector<Azienda> listaAziende = new Vector<Azienda>();
	double metodo[] = new double [3];

    CalcoloPIL() {
        insertAzienda();
        metodo[0]= metodoDeiRedditi();
		metodo[1]=metodoDellaSpesa();
		metodo[2]=metodoDelValoreAggiunto();
                int valScelta = 0;
		do{
			System.out.println("Con quale metodo vuoi calcolare il PIL ?");
			System.out.println("1 - Metodo dei Redditi");
                        System.out.println("2 - Metodo della Spesa");
                        System.out.println("3 - Metodo del Valore Aggiunto");
                        System.out.println("4 - Stampa tutti i metodi");
                        System.out.println("5 - Chiudi");
                        try{
                        valScelta = tastiera.nextInt();

			//1 - 2 - 3 - 4(tutti) - 5(chiudi) 
			if(valScelta == 4){
				for (int i = 0; i < metodo.length; i++) {
					System.out.println(metodo[i]);
				}
			} else if(valScelta>=1&&valScelta<=3){
				System.out.println(metodo[valScelta-1]);
			}else if(valScelta == 5){
				System.out.println("Chiusura software...");
			}else{
                            System.out.println("Metodo inesistente");
                        }
                        }catch (InputMismatchException e ){
                            System.out.println("Hai inserito un formato non valido, inserisci solamente un numero corrispondente alla lista");
                            valScelta = 5;
                        }
			
		}while(valScelta!=5);
    }

    void insertAzienda() {
        int numAziende;
        System.out.println("Quante aziende vuoi inserire ? ");
        try{
        numAziende = tastiera.nextInt();
        for (int i = 1; i < numAziende + 1; i++) {
            System.out.println("Inserisci nome azienda " + i + "?");
            String nomeAzienda = tastiera.next();
            System.out.println("Quali sono i salari dell'azienda " + nomeAzienda + " :");
            double salari = tastiera.nextDouble();
            System.out.println("Quali sono i salari dell'azienda " + nomeAzienda + " :");
            double profitti = tastiera.nextDouble();
            System.out.println("Qual'è il fatturato dell'azienda " + nomeAzienda + " :");
            double fatturato = tastiera.nextDouble();
            System.out.println("Questa azienda vende i suoi prodotti ad altre aziende? (S\\N)");
            String risposta = tastiera.next().trim().toUpperCase();
            boolean mercato = true;
            if (risposta.equals("S")) {
                mercato = false;
            }
            listaAziende.add(new Azienda(nomeAzienda, salari, profitti, fatturato, mercato));
        }
        }catch(InputMismatchException e){
            System.out.println("Hai inserito un dato non consono alle informazioni richieste");
            System.exit(-1);
        }
    }

    double metodoDellaSpesa() {
        double PIL = 0;
        for (Azienda azienda : listaAziende) {
            if (azienda.mercato == true) {
                PIL += azienda.fatturato;
            }
        }
        return PIL;
    }

    double metodoDelValoreAggiunto() {
        double PIL = 0;
        for (Azienda azienda : listaAziende) {
            PIL += azienda.fatturato - azienda.spesa;
        }
        return PIL;
    }

    double metodoDeiRedditi() {
        double PIL = 0;
        for (Azienda azienda : listaAziende) {
            PIL += azienda.salari + azienda.profitti;
        }
        return PIL;
    }

    public static void main(String[] args) {
        new CalcoloPIL();
    }

}



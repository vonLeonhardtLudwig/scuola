let bool = true

//creazione promise
let prom = new Promise(function(resolve, reject){
    if(bool){
        resolve("promise 1")
    }
})


let prom2 = new Promise(function(resolve, reject){
    if(bool){
        resolve("promise 2")
    }
})

let prim = new Promise((resolve,reject)=>{
    setTimeout(()=>{
        console.log("wait a second")
        resolve("promise timeout")
    }
    ,1000)
})

/*prom.then(function(res){
    //se viene soddisfatta la promise
    console.log("gotcha "+res)
}).catch(function(rej){
    //se non viene soddisfatta la promise
    console.log("not gotcha grr"+rej)
})*/

prim.then((res)=>{
    console.log(res +" here you go")  //terzo
}).then(prom.then((res)=>{
    console.log("gotcha "+res) //primo
})).then(()=>{
    console.log("no promise ") //quarto
}).then(prom2.then((res)=>{
    console.log("take "+ res) //secondo
})).then(()=>{
    console.log("whenn??") //quinto
})

/*OUTPUT:
gotcha promise 1
take promise 2
wait a second
promise timeout here you go
no promise
whenn??
*/ 


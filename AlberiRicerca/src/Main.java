import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
        BinaryTree b = new BinaryTree(new int[]{10,5,15,3,7,1,4,6,8,12,17,11,13,16,18});
        b.preOrder();
        System.out.println("<-- pre order");
        b.inOrder();
        System.out.println("<-- in order");
        b.postOrder();
        System.out.println("<-- post order");
    }
}

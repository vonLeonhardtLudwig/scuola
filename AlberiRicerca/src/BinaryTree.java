import javafx.geometry.Pos;

import java.util.Iterator;
import java.util.*;
import java.lang.UnsupportedOperationException;

public class BinaryTree {

    private Node root;
    //private Stack<Node> stack;
    //private Node actual;
    public void add(int key){
        if(root==null){
            root = new Node(key);
        }else{
            Node parent = root.parent;
            Node actual = root;
            while(actual!=null){
                parent=actual;
                actual = key<actual.key?actual.l:actual.r;
            }
            actual = new Node(key,parent);
            if(parent.key>key){
                parent.l=actual;
            }else{
                parent.r=actual;
            }
        }
    }

    public BinaryTree(){}

    public BinaryTree(int key){
        root = new Node(key);
    }

    public BinaryTree(int[]arr){
        for(int i = 0; i<arr.length; i++){
            add(arr[i]);
        }
    }

    private class Node{

        private Node l;
        private Node r;
        private Node parent;
        private int key;

        private Node(){}

        private Node(int key){
            this.key=key;
            this.l=null;
            this.r=null;
            this.parent=null;
        }
        private Node(int key, Node parent){
            this.key=key;
            this.parent=parent;
        }
        private Node(Node parent){
            this.parent=parent;
        }
        private Node(int key, Node l, Node r){
            this.key=key;
            this.l=l;
            this.r=r;
        }
        private Node(int key, Node l, Node r, Node parent){
            this.key=key;
            this.l=l;
            this.r=r;
            this.parent=parent;
        }
    }

    private static class PreOrder implements Iterator<Node> {
        //Node-l-r
        PreOrder(Node root){
            actual = root;
            stack = new Stack<>();
        }
        private Stack<Node> stack;
        private Node actual;
        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        @Override
        public Node next() {
            actual = stack.pop();
            return actual;
        }

    }
    private static class InOrder implements Iterator<Node>{
        InOrder(Node root){
            actual = root;
            stack = new Stack<>();
        }
        private Stack<Node> stack;
        private Node actual;
        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        @Override
        public Node next() {
            Node temp;
            actual = stack.pop();
            temp = actual;
            actual = actual.r;
            return temp;
        }
    }

    private static class PostOrder implements Iterator<Node>{
        PostOrder(Node root){
            actual = root;
            stack = new Stack<>();
        }
        private Stack<Node> stack;
        private Node actual;
        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        @Override
        public Node next() {
            actual = stack.pop();
            return actual;
        }
    }

    public void preOrder(){
        //Node-l-r
        if (root == null) {
            return;
        }
        PreOrder it = new PreOrder(root);
        it.stack.push(it.actual);
        while(it.hasNext()){
            it.next();
            System.out.print(it.actual.key + " ");
            if (it.actual.r != null)
                it.stack.push(it.actual.r);
            if (it.actual.l != null)
                it.stack.push(it.actual.l);

        }
    }


    public void inOrder(){
        //l-Node-r
        if (root == null)
            return;
        InOrder it = new InOrder(root);
        while (it.actual != null || it.hasNext())
        {
            while (it.actual !=  null)
            {
                it.stack.push(it.actual);
                it.actual = it.actual.l;
            }
            System.out.print(it.next().key+ " ");
        }
    }
    private void postRec(Node actual){
        if(actual!=null){
            System.out.print(actual.key+" ");
            postRec(actual.l);
            postRec(actual.parent.r);
            postRec(actual.parent);

        }
    }
    public void postOrder(){
        PostOrder it = new PostOrder(root);
        postRec(it.actual);
    }
/*
    public void postOrder(){
        //l-r-Node
        if (root == null)
            return;
        PostOrder it = new PostOrder(root);
        it.stack.push(it.actual);
        Node prev = null;
        while(it.hasNext()){

            System.out.print(it.actual.key+" ");
            if(it.actual.parent!=null)
                it.stack.push(it.actual.parent);
            if (it.actual.parent.r != null)
                it.stack.push(it.actual.parent.r);
            if (it.actual.l != null)
                it.stack.push(it.actual.l);
            it.next();
        }
    }*/

}

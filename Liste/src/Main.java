public class Main {

    public static void main(String[] args) {
        //test con lista inizialmente vuota
        BiList<Integer> lista = new BiList<>();
        lista.add(5);
        lista.add(34);
        lista.add(1377);
        lista.delete(0);
        lista.change(0,lista.getSize()-1);
        lista.printList();
        System.out.println();

        //test con lista data da un array
        BiList<String> nomiStudenti = new BiList<>(new String[]{"mario","luigi","yoshi","wario","waluigi"});
        nomiStudenti.delete(nomiStudenti.getSize()-1);
        nomiStudenti.insert(0,"Peach");
        nomiStudenti.insert(nomiStudenti.getSize()-1,"Daisy");
        System.out.println("Waluigi "+nomiStudenti.search("Waluigi"));
        nomiStudenti.printList();
    }
}

public class BiList<T> {

    private class Node{
        private Node prev;
        private Node next;
        private T key;

        private Node(){}

        private Node(T key){
            this.key = key;
        }

        private Node(T key, Node prev, Node next){
            this.key = key;
            this.prev = prev;
            this.next=next;
        }


    }

    private Node head;

    private Node tail;

    private int size;

    public BiList(){
        head = null;
        tail = null;
        size = 0;
    }

    public BiList(T i){
        head = new Node(i,null,null);
        tail = head;
        size = 1;
    }

    public BiList(T i, T j){
        head = new Node(i,null,null);
        tail = new Node(j,null,null);
        head.next=tail;
        tail.prev=head;
        size = 2;
    }

    public BiList(T[]arr){
        head = new Node(arr[0], null, null);
        Node actual = new Node();
        head.next=actual;
        actual.prev=head;
        int i = 1;
        size = 1;
        for(;i<arr.length-1;i++){
          actual.key=arr[i];
          actual.next= new Node(arr[i+1],actual,null);
          actual = actual.next;
          size++;
        }
        tail=actual;
        //tail = new Node(arr[arr.length-1],actual,null);
        size++;
    }

    public void add(T key){
        //inserimento in coda
        Node n = new Node(key,tail,null);
        if(size!=0){
        tail.next=n;
        tail = n;
        size++;
        }else{
            head=n;
            tail=n;
            size++;
        }
    }

    public void insert(int p, T key) throws UnsupportedOperationException{
        //GLI INDICI DEGLI ELEMENTI IN LISTA PARTONO DA 0
        //inserimento in testa
        if(p==0){
            Node n = new Node(key,null,head);
            head.prev=n;
            head = n;
            size++;
        }
        //inserimento in coda
        else if(p==size-1){
            Node n = new Node(key,tail,null);
            tail.next=n;
            tail = n;
            size++;
        }
        //inserimento in lista
        else if(p>0&&p<size){
            Node n = new Node(key);
            Node actual = head;
            int i = 0;
            for(;i<p;i++){
                actual = actual.next;
            }
            actual.prev.next=n;
            n.prev = actual.prev;
            n.next = actual;
            actual.prev = n;
            size++;
        }else{
            throw new UnsupportedOperationException("OUT OF BOUNDS");
        }
    }

    public int search(T key){
        int pos= 0;
        Node actual=head;
        for(;pos<size-1&&!actual.key.equals(key);pos++){
            actual=actual.next;
        }
        return actual.key.equals(key)?pos:-1;
    }

    public void change(int p1, int p2)throws UnsupportedOperationException {
        Node n1 = null;
        Node n2 = null;
        int tempSwap= p1;
        p1=p1>p2?p2:p1;
        p2=p2==p1?tempSwap:p2;
        for (int i = 0, actualPos = p1; i < 2; i++, actualPos = p2) {
            if (actualPos >= 0 && actualPos < size) {
                int j;
                Node actual;
                if (actualPos <= size / 2) {
                    actual = head;
                    j = 0;
                    for (; j < actualPos; j++) {
                        actual = actual.next;
                    }
                    if (actualPos == p1) {
                        n1 = actual;
                    } else {
                        n2 = actual;
                    }
                } else {
                    actual = tail;
                    j = size - 1;
                    for (; j > actualPos; j--) {
                        actual = actual.prev;
                    }
                    if (actualPos == p1) {
                        n1 = actual;
                    } else {
                        n2 = actual;
                    }
                }
            } else {
                throw new UnsupportedOperationException("OUT OF BOUNDS");
            }
        }
        if (size != 2 && (p1 == p2 - 1 || p2 == p1 - 1) && n1.prev != null && n2.next != null) {
            //nodi adiacenti non testa e coda
            n2.next.prev = n1;
            n1.prev.next = n2;
            n1.next = n2.next;
            n2.prev = n1.prev;
            n1.prev = n2;
            n2.next = n1;
        } else if (size == 2) {
            //testa e coda adiacenti
            tail.next = head;
            head.prev = tail;
            tail.prev = null;
            head.next = null;
            Node temp = head;
            head = tail;
            tail = temp;
        }else if((p1==0&&p2==size-1)||(p2==0&&p1==size-1)) {
            //testa e coda non adiacenti
            head.next.prev=tail;
            tail.prev.next=head;
            head.prev=tail.prev;
            tail.next=head.next;
            head.next=null;
            tail.prev=null;
            Node temp = head;
            head = tail;
            tail = temp;
        }else if((p1 == p2 - 1 || p2 == p1 - 1)&&(p1==0||p2==0)){
            //testa con nodo adiacente
            n1.next=n2.next;
            n2.next.prev=n1;
            n2.next=n1;
            n1.prev=n2;
            n2.prev=null;
            head=n2;
            n2=n1;
        }else if(p1==0||p2==0) {
            //testa con nodo non adiacente
            Node temp1=n1.next;
            Node temp2=n2.next;
            n1.next=temp2;
            n2.next=temp1;
            n1.prev=n2.prev;
            n2.prev.next=n1;
            temp2.prev=n1;
            temp1.prev=n2;
            n2.prev=null;
            head=n2;
            n2=n1;
        }else if((p1 == p2 - 1 || p2 == p1 - 1)&&(p1==size-1||p2==size-1)){
            //nodo con coda adiacente
            n1.prev.next=n2;
            n2.prev=n1.prev;
            n2.next=n1;
            n1.prev=n2;
            n1.next=null;
            tail=n1;
            n1=n2;
        }else if((p1==size-1||p2==size-1)) {
            //nodo con coda non adiacente
            Node temp1=n1.prev;
            Node temp2=n2.prev;
            n1.prev=n2.prev;
            n2.prev=n1.prev;
            temp1.next=n2;
            temp2.next=n1;
            n1.next.prev=n2;
            n2.next=n1.next;
            n1.next=null;
            tail=n1;
            n1=n2;
        }else {
            //nodo con nodo
            n2.next.prev=n1;
            n2.prev.next=n1;
            n1.next.prev=n2;
            n1.prev.next=n2;
            Node temp = n1.next;
            n1.next = n2.next;
            n2.next=temp;
            temp=n1.prev;
            n1.prev=n2.prev;
            n2.prev=temp;
        }
    }

    public T getVal(int p)throws UnsupportedOperationException{
        if(p>=0&&p<size){
            int i;
            Node actual;
            if(p<=size/2){
                actual = head;
                i = 0;
                for(;i<p;i++){
                    actual=actual.next;
                }
                return actual.key;
            }else{
                actual = tail;
                i = size-1;
                for(;i>p;i--){
                    actual=actual.prev;
                }
                return actual.key;
            }
        }else{
            throw new UnsupportedOperationException("OUT OF BOUNDS");
        }
    }

    public int getSize(){
        return size;
    }

    public T delete(int p)throws UnsupportedOperationException{

        if(p>=0&&p<size){

            int i;
            Node actual;

            if(p == 0){
                if(size>1){
                    head=head.next;
                    head.prev=null;
                    size--;
                    return head.key;
                }else{
                    head=null;
                    tail=null;
                    size--;
                    return null;
                }
            }else if(p==size-1){
                tail=tail.prev;
                tail.next= null;
                size--;
                return head.key;
            }else if(p<=size/2){
                actual = head;
                i = 0;
                for(;i<p;i++){
                    actual=actual.next;
                }
                actual.prev.next=actual.next;
                actual.next.prev=actual.prev;
                return actual.key;
            }else{
                actual = tail;
                i = size-1;
                for(;i>p;i--){
                    actual=actual.prev;
                }
                actual.prev.next=actual.next;
                actual.next.prev=actual.prev;
                return actual.key;
            }
        }else{
            throw new UnsupportedOperationException("OUT OF BOUNDS");
        }
    }

    public void printList()throws UnsupportedOperationException{
        Node actual = head;
        int i = 0;
        if(size>0){
        while(actual!=null){
            System.out.println(actual.key);
            actual = actual.next;
        }
        }else{
            throw new UnsupportedOperationException("EMPTY LIST");
        }
    }

}

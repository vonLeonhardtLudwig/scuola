package com.example.rgbslider;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {
    private int seekR, seekG, seekB;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SeekBar sbR =  findViewById(R.id.seekBarRed);
        SeekBar sbG =  findViewById(R.id.seekBarGreen);
        SeekBar sbB =  findViewById(R.id.seekBarBlue);
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        sbR.setOnSeekBarChangeListener(sbRsetOnSeekBarChangeListener);
        sbG.setOnSeekBarChangeListener(sbGsetOnSeekBarChangeListener);
        sbB.setOnSeekBarChangeListener(sbBsetOnSeekBarChangeListener);

    }
    private SeekBar.OnSeekBarChangeListener sbRsetOnSeekBarChangeListener
            = new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekR = progress;
                slideColor();
            }
        };

    private SeekBar.OnSeekBarChangeListener sbGsetOnSeekBarChangeListener
            = new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekG = progress;
                slideColor();
            }
        };

    private SeekBar.OnSeekBarChangeListener sbBsetOnSeekBarChangeListener
            = new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekB = progress;
                slideColor();
            }
        };



    private void slideColor(){
        LinearLayout circle = findViewById(R.id.circle);
        int color = Color.rgb(seekR, seekG, seekB);
        GradientDrawable shapeDrawable = (GradientDrawable)circle.getBackground();
        shapeDrawable.setColor(color);
    }


}

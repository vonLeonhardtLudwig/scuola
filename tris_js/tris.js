let tabella = new Table()
let moves = 0
function Table(){
  this.create_matrix = function(){
    let m = []
    for(let y = 0;y<3;y++){
      m[y] =[];
      for(let x = 0; x<3; x++){
        m[y][x]=null;
      }
    }
    return m
  }
  this.matrix = this.create_matrix()
  this.initialize_table = function(){
    let rows = document.getElementsByClassName('row')
    for(let i = 0; i<3;i++){
        for(let j = 0; j<3; j++){
          this.matrix[i][j]=new Cell(rows[i].children[j],i,j)
        }
    }
  }
}

function Cell(cell,riga,colonna){
  this.simbol = ""
  this.cell = cell
  this.row = riga
  this.column = colonna
  this.hasBeenClicked = false
  this.cell.onclick = function(event){
    this.row = riga
    this.column = colonna
    if(!this.hasBeenClicked){
      console.log(this.row,this.column)
      //set del simbolo
      this.hasBeenClicked=true
      console.log(this.hasBeenClicked)
      let s = document.createElement("i")
      s.setAttribute("class","material-icons")
      //#1de9b6 verde acqua
      this.simbol = moves++%2==0?"x":"o"
      console.log(this.simbol)
      if(this.simbol == 'x'){
          s.innerHTML = "cancel"
      }else{
          s.innerHTML = "trip_origin"
      }
      cell.appendChild(s)

      //controllo se ha vinto
      //console.log(tabella.matrix[riga][0], this.simbol, tabella.matrix[riga][0].simbol==this.simbol)
      if(tabella.matrix[riga][0].simbol==this.simbol&&tabella.matrix[riga][1].simbol==this.simbol&&tabella.matrix[riga][2].simbol==this.simbol){//riga
          console.log(this.simbol +" won")
      }
    }
  }
}

window.onload = function(){
  tabella.initialize_table()
}

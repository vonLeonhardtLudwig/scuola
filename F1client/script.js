//creazione header di una tabella (essendo che tutte e tre le tabelle visualizzate condividono gli stessi dati)
function createHeader(id_tabella){
	let header_els = ["ID Pilota","Cognome Pilota","Nome Pilota","Nazionalita","Data di Nascita"]
	let tab = $(id_tabella)
	console.log(tab)
	$.each(header_els, function(ix, val) { 
		tab.append("<th>"+val+"</th>")
	})
}

$(document).ready(function(){
	//TEST SERVIZIO REST
	$("#testServizio").click(function(){
		$.ajax({
			url: "http://127.0.0.1:3000/",
			type: "GET",
			contentType:"application/json", 
			success:function(result){
				$("#testResults").append(result)
			},
			error:function(){alert("ERRORE")}
		})
	})
	
	//ELENCO COSTRUTTORI (dati grezzi)
	$("#elencoCostruttori").click(function(){
		$.ajax({
			url: "http://127.0.0.1:3000/",
			type: "GET",
			contentType:"application/json", 
			success:function(result){
				$("#constructorsResult").append(JSON.stringify(result))
			},
			error:function(){alert("ERRORE")}
		})		
	})
	
	//ELENCO COSTRUTTORI (lista non ordinata)
	$("#listaCostruttori").click(function(){
		$.ajax({
			url: "http://127.0.0.1:3000/",
			type: "GET",
			contentType:"application/json", 
			success:function(result){
				//ul e li
				//nome e la nazione di un costruttore 
				//nomePilota cognomePilota nazionalita
				
				let lista = $("<ul>")
				for(let i = 0; i<result.length;i++){
					lista.append("<li>"+result[i].nomeCostruttore+" Nazione: "+result[i].nazioneCostruttore+"</li>")
				}
				$("#listaCostruttori_result").append(lista)
			},
			error:function(){alert("ERRORE")}
		})
	})
	
	//TABELLA
	$("#tabellaPiloti").click(function(){
		$.ajax({
			url: "http://127.0.0.1:3000/",
			type: "GET",
			contentType:"application/json", 
			success:function(result){
				createHeader("#tabellaTuttiPiloti")
				let tab = $("#tabellaTuttiPiloti")
				for(let row = 0; row<result.length;row++){
					let tr = $("<tr>")
					$.each(result[row], function(propName, propVal) { 
						tr.append("<td>"+propVal+"</td>")
					})
					$("#tabellaTuttiPiloti").append(tr)
				}
			},
			error:function(){alert("ERRORE")}
		})
	})
	
	//TEXTBOX
	$("#scegliTeam").click(function(){
		let index = $("#formTeam").val()
		console.log(index)
		$.ajax({
			url: "http://127.0.0.1:3000/"+index,
			type: "GET",
			contentType:"application/json", 
			success:function(result){
				createHeader("#tabellaPilotiTeamsForm")
				let tab = $("#tabellaPilotiTeamsForm")
				for(let row = 0; row<result.length;row++){
					let tr = $("<tr>")
					$.each(result[row], function(propName, propVal) { 
						tr.append("<td>"+propVal+"</td>")
					})
					$("#tabellaPilotiTeamsForm").append(tr)
				}
			},
			error:function(){alert("ERRORE")}
		})
	})
	//SELECTOR
	/*
	*DOPO IL CARICAMENTO DEL DOCUMENT, PARTE UNA RICHIESTA AJAX PER CARICARE NEL SELETTORE I VARI ELEMENTI 
	*RESTITUITI DALL'API.
	*SUCESSIVAMENTE CI SAR� LA GESTIONE DELLA RICHIESTA AJAX TRAMITE SELETTORE
	*/
	$.ajax({
		url: "http://127.0.0.1:3000/",
		type: "GET",
		contentType:"application/json", 
		success:function(result){
			let sel = $("#teamSelector")
			for(let i = 0; i<result.length;i++){
				let opt = $("<option>")
				opt.text(result[i].nomeCostruttore)
				opt.val(result[i].idCostruttore)
				sel.append(opt)
			}
		},
		error:function(){alert("ERRORE")}
	})
	
	$("#teamSelector").change(function(){
		let index = $("#teamSelector").val()
		console.log(index)
		$.ajax({
			url: "http://127.0.0.1:3000/"+index,
			type: "GET",
			contentType:"application/json", 
			success:function(result){
				createHeader("#tabellaPilotiTeamsSelettore")
				let tab = $("#tabellaPilotiTeamsSelettore")
				for(let row = 0; row<result.length;row++){
					let tr = $("<tr>")
					$.each(result[row], function(propName, propVal) { 
						tr.append("<td>"+propVal+"</td>")
					})
					$("#tabellaPilotiTeamsSelettore").append(tr)
				}
			},
			error:function(){alert("ERRORE")}
		})
	})
	
})